<?php
class CommentsController extends AppController {
    public $helpers = array('Html', 'Form');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('add');
    }

    public function add($post_id = null) {
        if ($this->request->is('post')) {
                $this->Comment->set(array('post_id'=>$post_id));
            if ($this->Comment->save($this->request->data)) {
                $this->Flash->success(__('Your comment has been posted.'));
                return $this->redirect(array('controller' => 'posts', 'action' => 'view', $post_id));
            }
            $this->Flash->error(__('Unable to add your comment.'));
        }
    }

}