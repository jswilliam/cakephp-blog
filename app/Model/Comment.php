<?php
class Comment extends AppModel {
    public $validate = array(
        'name' => array(
            'rule' => 'notBlank'
        ),
        'email' => array(
            'rule' => 'notBlank'
        ),
        'text' => array(
            'rule' => 'notBlank'
        )
    );
}