<div class="categories form">
<?php echo $this->Form->create('Category'); ?>
    <fieldset>
        <legend><?php echo __('Add Category'); ?></legend>
        <div class="form-group">
        <?php echo $this->Form->create('Category'); ?>
        </div>
        <div class="form-group">
        <?php echo $this->Form->input('name',  array('class' => 'form-control')); ?>
        </div>        
        <div class="form-group">
        <?php echo $this->Form->input('id', array('type' => 'hidden', 'class' => 'form-control'));?>
        </div>        
    </fieldset>
    <?php 
        $options = array('label' => 'Submit', 'class' => 'btn btn-primary', 'div' => false);
        echo $this->Form->end($options); 
    ?>
</div>