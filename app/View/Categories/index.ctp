<h2>Categories</h2>
<p><?php echo $this->Html->link('Add Category', array('action' => 'add')); ?></p>
<table class="table table-striped">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Actions</th>
        <th>Created</th>
    </tr>

    <?php foreach ($categories as $category): ?>
    <tr>
        <td><?php echo $category['Category']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $category['Category']['name'],
                    array('action' => 'view', $category['Category']['id'])
                );
            ?>
        </td>
        <td>
            <?php
                echo $this->Form->PostLink(
                    'Delete',
                    array('action' => 'delete', $category['Category']['id']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
            <?php
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $category['Category']['id'])
                );
            ?>
        </td>
        <td>
            <?php echo $category['Category']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>