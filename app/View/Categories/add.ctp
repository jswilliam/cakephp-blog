<div class="categories_form">
<?php echo $this->Form->create('Category'); ?>
    <fieldset>
        <legend><?php echo __('Add Category'); ?></legend>
        <?php echo $this->Form->create('Category'); ?>
        <div class="form-group">
            <?php echo $this->Form->input('name', array('class' => 'form-control')); ?>
        </div>
    </fieldset>
    <?php 
        $options = array('label' => 'Submit', 'class' => 'btn btn-primary', 'div' => false);
        echo $this->Form->end($options); 
    ?>
</div>