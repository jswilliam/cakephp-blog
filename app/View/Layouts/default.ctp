<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
		<?php echo $this->Html->charset(); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		echo $this->Html->css('bootstrap.min.css');
 		echo $this->Html->script('bootstrap.min.js');
	?>
</head>
<body>
 <nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
				<!-- <a class="navbar-brand" href="http://www.deemalab.com/">DeemaLab CakePHP Challenge</a> -->
				</div>
				<ul class="nav navbar-nav">
				<li><?php echo $this->Html->link('Register', array('controller' => 'users', 'action' => 'add')); ?></li>
				<li><?php echo $this->Html->link('Posts', array('controller' => 'posts', 'action' => 'index')); ?></li>
				<li><?php echo $this->Html->link('Ctaegories', array('controller' => 'categories', 'action' => 'index')); ?></li>
				<li><?php echo $this->Html->link('Users', array('controller' => 'users', 'action' => 'index')); ?></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><?php
					if(AuthComponent::user()) {
					// user is logged in, show logout..user menu etc
					echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout')); 
					} else {
					// the user is not logged in
					echo $this->Html->link('Login', array('controller' => 'users', 'action' => 'login')); 
					}
				?></li>
				</ul>
			</div>
		</nav> 
	<div id="container" style="width: 80%; margin: 0 auto">
		<div id="content">	
			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
		</div>
	</div>
</body>
</html>
