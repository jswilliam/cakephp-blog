<div class="users_form">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <div class="alert alert-info">
             <?php echo __('Please enter your username and password'); ?>
        </div>
        <?php echo $this->Form->input('username', array('class' => 'form-control'));
        echo $this->Form->input('password', array('class' => 'form-control'));
    ?>
    </fieldset>
    <br>
    <?php 
        $options = array('label' => 'Login', 'class' => 'btn btn-primary', 'div' => false);
        echo $this->Form->end($options); 
    ?>
</div>