<div class="users form">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
        <div class="form-group">
        <?php 
        echo $this->Form->input('username', array('class' => 'form-control'));
        echo $this->Form->input('email', array('class' => 'form-control'));
        echo $this->Form->input('password', array('class' => 'form-control'));
        echo $this->Form->input('role', array(
            'options' => array('admin' => 'Admin', 'author' => 'Author'), 'class' => 'form-control'
        ));
    ?>
        </div>    
    </fieldset>
    <?php 
        $options = array('label' => 'Submit', 'class' => 'btn btn-primary', 'div' => false);
        echo $this->Form->end($options); 
    ?>
</div>