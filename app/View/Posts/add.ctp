<div class="posts form">
<?php echo $this->Form->create('Post'); ?>
    <fieldset>
        <legend><?php echo __('Add Post'); ?></legend>
    <div class="form-group">
        <?php 
        echo $this->Form->create('Post', array('class' => 'form-control'));
        echo $this->Form->input('title', array('class' => 'form-control'));
        echo $this->Form->input('body', array('rows' => '3', 'class' => 'form-control'));
        echo $this->Form->input('category_id', array('class' => 'form-control'));
    ?>
    </div>
    </fieldset>
    <?php 
        $options = array('label' => 'Submit', 'class' => 'btn btn-primary', 'div' => false);
        echo $this->Form->end($options); 
    ?>
</div>