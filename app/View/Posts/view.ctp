<h2><?php echo h($post['Post']['title']); ?></h2>

<p><small>Created: <?php echo $post['Post']['created']; ?></small></p>
<p><small>User: <?php echo $this->Html->link(
                    $post['User']['username'],
                    array('controller'=>'users', 'action' => 'view', $post['User']['id'])
                ); ?></small></p>
<p><small>Category: <?php echo $this->Html->link(
                    $post['Category']['name'],
                    array('controller'=>'categories', 'action' => 'view', $post['Category']['id'])
                ); ?></small></p>
<p><b><?php echo h($post['Post']['body']); ?></b></p>
<?php foreach ($post['Comment'] as $comment): ?>
    <hr>
    <p>Name: <?php print_r($comment['name'])?></p>
    <p>Email: <?php print_r($comment['email'])?></p>
    <b><?php print_r($comment['text'])?></b>
    <small><?php print_r($comment['created'])?></small>    
    
<?php endforeach; ?>
<hr><br>
<?php echo $this->Form->create('Comment', array('url'   => array( 'controller' => 'comments','action' => 'add', $post['Post']['id'] ))); ?>
        <legend><h3><?php echo __('Add Comment'); ?></h3></legend>
        <?php 
        echo $this->Form->create('Comment');
        echo $this->Form->input('name', array('class' => 'form-control'));        
        echo $this->Form->input('email', array('class' => 'form-control'));
        echo $this->Form->input('text', array('rows' => '3','class' => 'form-control'));
    ?><br>
    <?php 
        $options = array('label' => 'Comment', 'class' => 'btn btn-primary', 'div' => false);
        echo $this->Form->end($options); 
    ?><br>
